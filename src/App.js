import React from 'react';
import './App.scss';
import AnimatedText from "./components/AnimatedText"
import StyledError from "./components/StyledError";
import Pancakes from "./components/Pancakes";

class App extends React.Component {
  state = {
    numOfPancakes: '',
    isStarted: false,
    showError: false,
    isSorted: false,
    animationTimeInterval: 1000,
  };

  startTheShow = (e) => {
    e.preventDefault();
    const { numOfPancakes } = this.state;

    this.setState({ isSorted: false });

    if (Number(numOfPancakes >= 2 && numOfPancakes <= 50)) {
      this.setState({ isStarted: true, showError: false });
    } else {
      this.setState({ showError: true, isStarted: false });
    }
  };

  componentDidMount() {
    this.animationTimeInput.focus();
  }

  handlePancakesCountChange = (e) => {
    this.setState({ numOfPancakes: e.target.value });
  };

  handleAnimationTimeChange = (e) => this.setState({animationTimeInterval: e.target.value});

  reset = () => {
    this.setState({ isStarted: false, isSorted: false });
  };

  showResult = () => {
    this.setState({ isStarted: false, isSorted: true });
  };

  render() {
    const { isStarted, numOfPancakes, showError, isSorted, animationTimeInterval } = this.state;

    return (
        <React.Fragment>
          {!isStarted && <div className="title">This is a simple, animated visualizer of the famous <a
              href="https://en.wikipedia.org/wiki/Pancake_sorting">Pancake Sorting algorithm</a>.</div>}
          <form onSubmit={this.startTheShow}>
            <input disabled={isStarted} type="number" onChange={this.handleAnimationTimeChange} className="animation-time" required
                   placeholder="Time (ms)"
                   ref={(input) => {
                     this.animationTimeInput = input;
                   }}
            />
            <input disabled={isStarted} type="number" onChange={this.handlePancakesCountChange} required
                   placeholder="How many pancakes [from 2 to 50]*"
            />
            <button disabled={isStarted} onClick={this.startTheShow}>Action</button>
            <button disabled={!isStarted} onClick={this.reset} name="Reset">Reset</button>
          </form>
          {showError && <StyledError>Please enter valid number between 2 and 50 (including)</StyledError>}
          {isStarted && <Pancakes
              numOfPancakes={Number(numOfPancakes)}
              resultsCallack={this.showResult}
              animationTimeInterval={Number(animationTimeInterval)}/>}
          {isSorted && <div className="result">
            <AnimatedText text={'Sorted!'}/>
          </div>}
        </React.Fragment>
    );
  }
}

export default App;