import PropTypes from 'prop-types';
import React from 'react';
import posed, { PoseGroup } from 'react-pose';
import pancake from '../utils/pancake-algo';
import { isSorted, randomize } from "../utils/utils";
import './Pancakes.scss';

const Item = posed.li({});

class Pancakes extends React.Component {
  state = { items: [], isSorted: false };

  componentDidMount() {
    const { numOfPancakes } = this.props;

    if (numOfPancakes && Number(numOfPancakes) > 0) {

      this.calculatePancakeHeight(numOfPancakes);

      this.setState({ items: randomize(this.props.numOfPancakes) });
      this.interval = setInterval(this.toggleVisibility, this.props.animationTimeInterval);
    } else {
      console.error('Invalid value.')
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  componentDidUpdate() {
    if (this.state.isSorted) {
      this.props.resultsCallack();
    }
  }

  toggleVisibility = () => {
    if (isSorted(this.state.items)) {
      clearInterval(this.interval);
      this.setState({ isSorted: true })
    } else {
      this.setState({ items: pancake(this.state.items) })
    }
  };

  calculatePancakeHeight = (numOfPancakes) => {
    if (numOfPancakes > 10) {
      return Math.round(window.innerHeight / numOfPancakes) - 10;
    } else {
      return 35;
    }
  };

  render() {
    const { items, isSorted } = this.state;
    const { numOfPancakes } = this.props;

    return (
        <React.Fragment>
          {!isSorted &&
          <React.Fragment>
            <ul className="stage">
              <PoseGroup>
                {items.map((item) => (
                    <Item className="item"
                          style={{ width: 20 * item, height: this.calculatePancakeHeight(numOfPancakes) }}
                          data-key={item} key={item}/>)
                )}
              </PoseGroup>
            </ul>
          </React.Fragment>
          }
        </React.Fragment>
    );
  }
}

Pancakes.propTypes = {
  numOfPancakes: PropTypes.number.isRequired,
  resultsCallack: PropTypes.func.isRequired,
  animationTimeInterval: PropTypes.number.isRequired
};

export default Pancakes;