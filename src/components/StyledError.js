import styled from 'styled-components';

const StyledError = styled.div`
  font-size: 1.1em;
  color: red;
`;
export default StyledError;