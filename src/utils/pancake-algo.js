import { isSorted } from "./utils";

const process = (stack) => {
  const len = stack.length;

  if (len <= 1) {
    console.error("Invalid input: not enough pancakes in the stack.");
    return [];
  }

  if (isSorted(stack)) {
    return stack;
  }

  for (let i = len - 1; i >= 1; i--) {
    let j;
    let largestPancakeIdx = 0;
    let largestPancakeValue = stack[ 0 ];
    let newStack = [];

    for (j = 1; j <= i; j++) {
      if (stack[ j ] > largestPancakeValue) {
        largestPancakeValue = stack[ j ];
        largestPancakeIdx = j;
      }
    }

    if (largestPancakeIdx === i)
      continue;

    if (largestPancakeIdx > 0) {
      newStack = stack.slice(0, largestPancakeIdx + 1).reverse();

      for (j = 0; j <= largestPancakeIdx; j++) {
        stack[ j ] = newStack[ j ];
      }
    }

    newStack = stack.slice(0, i + 1).reverse();
    for (j = 0; j <= i; j++) {
      stack[ j ] = newStack[ j ];
    }

    return stack;
  }

  return stack;
};

export default process;