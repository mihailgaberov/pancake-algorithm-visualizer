function shuffle(arr) {
  let currentIndex = arr.length,
    temporaryValue,
    randomIndex

  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    temporaryValue = arr[currentIndex];
    arr[currentIndex] = arr[randomIndex];
    arr[randomIndex] = temporaryValue;
  }

  return arr;
}

export function isSorted(arr) {
  return !!arr.reduce((memo, item) => memo && item >= memo && item);
}

/*
* Gets a number and generate and array with that length, containing
* random sized pancakes
*
* number must be between 2 and 50
* */
export function randomize(numOfPancakes) {
  if (numOfPancakes < 2 || numOfPancakes > 50) {
    throw new Error('Please enter a number between 2 and 50.');
  }

  return shuffle(Array.from(new Array(numOfPancakes), (val, index) => index + 1));
}